/*
* SPDX-FileCopyrightText: 2024 Kåre Särs <kare.sars@iki.fi>
* SPDX-License-Identifier: GPL-2.0-or-later
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program in a file called COPYING; if not, write to
* the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/
#include "MobileHelpers.h"

#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#include <QJniEnvironment>
#include <QJniObject>
#include <QFont>
#include <QScreen>
#include <QDebug>
#endif

class MobileHelpers::Private {
public:
    QColor statusbarColor;
    QColor navigationbarColor;
};


#ifdef Q_OS_ANDROID
#define FLAG_TRANSLUCENT_STATUS 0x04000000
#define FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS 0x80000000
#define SYSTEM_UI_FLAG_LIGHT_STATUS_BAR 0x00002000

void MobileHelpers::setStatusbarColor(const QColor& color)
{
    d->statusbarColor = QColor(color);
    int colorSum = d->statusbarColor.red() + d->statusbarColor.green() + d->statusbarColor.blue();
    bool lightBg = colorSum > (3*255)/2;

    QNativeInterface::QAndroidApplication::runOnAndroidMainThread([&]() {
        QJniObject activity = QNativeInterface::QAndroidApplication::context();
        if (!activity.isValid()) return;
        auto window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
        if (!window.isValid()) return;
        // Set color
        window.callMethod<void>("setStatusBarColor", "(I)V", d->statusbarColor.rgba());

        // Set light/dark theme

        // This might work in the future to force dark/ligght mode, but right now,
        // androidx libraries are not supported by the current Qt android integration
        // constexpr int MODE_NIGHT_NO = 1;
        // constexpr int MODE_NIGHT_YES = 2;
        // QJniObject::callStaticObjectMethod("androidx/appcompat/app/AppCompatDelegate", "setDefaultNightMode","(I)V", MODE_NIGHT_YES);


        // This does not work as it needs to pass non-trivial types, which is not supported by the QJni interface
        // WindowInsetsControllerCompat windowInsetsController = WindowCompat.getInsetsController(getWindow(), getWindow().getDecorView());
        // windowInsetsController.setAppearanceLightStatusBars(true);


        // This does not work with newer androids
        //
        // QJniObject view = window.callObjectMethod("getDecorView", "()Landroid/view/View;");
        // int visibility = view.callMethod<int>("getSystemUiVisibility", "()I");
        // if (lightBg)
        //     visibility |= SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
        // else
        //     visibility &= ~SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
        // view.callMethod<void>("setSystemUiVisibility", "(I)V", visibility);
    }).waitForFinished();

    emit statusbarColorChanged();
}


void MobileHelpers::setNavigationbarColor(const QColor& color)
{
    d->navigationbarColor = QColor(color);
    QNativeInterface::QAndroidApplication::runOnAndroidMainThread([&]() {
        QJniObject activity = QNativeInterface::QAndroidApplication::context();
        if (!activity.isValid()) return;
        auto window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
        if (!window.isValid()) return;
        window.callMethod<void>("setNavigationBarColor", "(I)V", d->navigationbarColor.rgba());
    }).waitForFinished();

    emit navigationbarColorChanged();
}

bool MobileHelpers::systemIsDarkMode()
{
    auto context = QJniObject(QNativeInterface::QAndroidApplication::context());
    // In Java:
    constexpr int UI_MODE_NIGHT_MASK = 0x30;
    constexpr int UI_MODE_NIGHT_YES = 0x20;
    //   context.getResources().getConfiguration().uiMode
    auto resources = context.callObjectMethod("getResources", "()Landroid/content/res/Resources;");
    auto config = resources.callObjectMethod("getConfiguration", "()Landroid/content/res/Configuration;");
    auto uiMode = config.getField<jint>("uiMode");
    return (uiMode & UI_MODE_NIGHT_MASK) == UI_MODE_NIGHT_YES;
}

void MobileHelpers::keepScreenOn(bool on) {
    QNativeInterface::QAndroidApplication::runOnAndroidMainThread([&]() {
        QJniObject activity = QNativeInterface::QAndroidApplication::context();
        if (activity.isValid()) {
            auto window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
            if (window.isValid()) {
                const int FLAG_KEEP_SCREEN_ON = 128;
                if (on) {
                    window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
                } else {
                    window.callMethod<void>("clearFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
                }
            }
        }
    }).waitForFinished();
}

void MobileHelpers::setApplicationFontSize(int pixelSize) {
    QNativeInterface::QAndroidApplication::runOnAndroidMainThread([&]() {
        QJniObject activity = QNativeInterface::QAndroidApplication::context();
        if (activity.isValid()) {
            QJniObject resources = activity.callObjectMethod("getResources","()Landroid/content/res/Resources;");
            if (resources.isValid()) {
                QJniObject configuration = resources.callObjectMethod("getConfiguration","()Landroid/content/res/Configuration;");
                float scale = configuration.getField<float>("fontScale");
                qDebug() << "Android Font-Scale: " << scale;
                int scaledPixelSize = qRound(pixelSize * scale);
                QFont font = QGuiApplication::font();
                font.setPixelSize(scaledPixelSize);
                QGuiApplication::setFont(font);
            }
        }
    }).waitForFinished();
}

#else
void MobileHelpers::setStatusbarColor(const QColor&) {}
void MobileHelpers::setNavigationbarColor(const QColor&) {}
bool MobileHelpers::systemIsDarkMode() { return true; }
void MobileHelpers::keepScreenOn(bool) {}
void MobileHelpers::setApplicationFontSize(int) {}
#endif


MobileHelpers::MobileHelpers(QObject* parent) : QObject(parent), d(new Private()) {}
MobileHelpers::~MobileHelpers() { delete d; }


QColor MobileHelpers::statusbarColor()
{
    return d->statusbarColor.name(QColor::HexArgb);
}

QColor MobileHelpers::navigationbarColor()
{
    return d->navigationbarColor.name(QColor::HexArgb);
}

