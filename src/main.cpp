/*******************************************************************
 *
 * Copyright 2024 Kåre Särs <kare.sars@iki.fi>
 * Copyright 2011-2012 Michael Nosov <Michael.Nosov@gmail.com>
 *
 * This file is part of the QML project "Reversi on QML"
 *
 * "Reversi on QML" is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * "Reversi on QML" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Reversi on QML"; see the file COPYING.  If not, write to
 * the Free Software Foundation, 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 ********************************************************************/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QTranslator>
#include <QUrl>
#include <QDebug>
#include "gameengine.h"
#include "MobileHelpers.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setOrganizationName("Sars Productions");
    app.setApplicationName("N9Reversi");

    qmlRegisterUncreatableType<GameEngine>("Reversi", 1, 0, "GameEngine", "should not be created there");
    qmlRegisterUncreatableType<Defs>("Reversi", 1, 0, "Defs", "should not be created there");
    qmlRegisterType<MobileHelpers>("sars.MobileHelpers", 1, 0, "MobileHelpers");

    GameEngine gameEng;
    BoardModel* gameModel = gameEng.gameModel();
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("gameEngine", &gameEng);
    engine.rootContext()->setContextProperty("gameModel", (QObject*)gameModel);
    engine.load(QUrl(QStringLiteral("qrc:qml/MainScreenItem.qml")));

    return app.exec();
}
