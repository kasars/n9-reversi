**Privacy Policy**

Hockey Speak is an Open Source application. The application itself does not collect any information about the user. Settings are stored only on the device.

Any usage statistics Google may collect, is Google's responsibility.
