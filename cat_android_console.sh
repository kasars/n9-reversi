#!/bin/sh

APP_PID=$(adb shell pidof org.sars.reversi)

adb logcat --pid $APP_PID
