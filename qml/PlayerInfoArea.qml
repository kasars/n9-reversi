/*******************************************************************
 *
 * Copyright 2011-2012 Michael Nosov <Michael.Nosov@gmail.com>
 *
 * This file is part of the QML project "Reversi on QML"
 *
 * "Reversi on QML" is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * "Reversi on QML" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Reversi on QML"; see the file COPYING.  If not, write to
 * the Free Software Foundation, 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 ********************************************************************/
import QtQuick
import Reversi 1.0
import "UIConstants.js" as UI

Item {
    id: playerCountInfo
    property int color: Defs.White
    property double chipSize: imgContent.implicitWidth
    height: playerTitle.height + countInfo.height + 20*UI.PLATFORM_SCALE_FACTOR

    Rectangle {
        anchors{
            fill: parent
            margins: 5*UI.PLATFORM_SCALE_FACTOR
        }
        color:"#00000000"
        border.color: "black"
        visible: gameEngine.curPlayer == playerCountInfo.color
        radius: 10*UI.PLATFORM_SCALE_FACTOR
    }

    Text {
        id: playerTitle
        anchors {
            top: parent.top
            topMargin: 10*UI.PLATFORM_SCALE_FACTOR
            horizontalCenter: parent.horizontalCenter
        }
        text: rootWindow.getShortStringForSkill(playerCountInfo.color == Defs.White? gameEngine.isWhiteHuman: gameEngine.isBlackHuman,
                                                playerCountInfo.color == Defs.White? gameEngine.whiteSkill: gameEngine.blackSkill)
        font.pixelSize: 19*UI.PLATFORM_SCALE_FACTOR
        color: "white"
    }

    Row {
        id: countInfo
        //width: playerInfo.width + imgContent.width + 5
        height: imgContent.height
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 10*UI.PLATFORM_SCALE_FACTOR
        }
        spacing: 5*UI.PLATFORM_SCALE_FACTOR
        Image {
            id: imgContent
            source: playerCountInfo.color == Defs.White? "qrc:/images/bw12.svg": "qrc:/images/bw1.svg"
            width: chipSize
            height: width
        }
        Text {
            id: playerInfo
            anchors.verticalCenter: imgContent.verticalCenter
            text: {
                return playerCountInfo.color == Defs.White? gameEngine.whiteCount: gameEngine.blackCount;
            }

            font.pixelSize: 20*UI.PLATFORM_SCALE_FACTOR
            color: "white"
        }
    }

    Rectangle {
        anchors{
            fill: parent
            margins: 5*UI.PLATFORM_SCALE_FACTOR
        }
        color: "#55cbb771"
        radius: 5*UI.PLATFORM_SCALE_FACTOR
        visible: marea.pressed
    }

    MouseArea {
        id: marea
        anchors.fill: parent
        onClicked: {
            console.log("Change player type");
            rootWindow.showSelectionDialog(playerCountInfo.color, 0, 0);
        }
    }
}
